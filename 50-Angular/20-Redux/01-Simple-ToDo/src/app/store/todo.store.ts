import {DevToolsExtension} from '@angular-redux/store';
import {createStore, applyMiddleware, compose} from 'redux';
import {createLogger} from 'redux-logger';
import {ToDo} from '../todos/model/todo.model';

export interface IAppState {
  pendingTodos: ToDo[];
}

const defaultState = {
  pendingTodos: [
    {
      title: 'Learn JavaScript',
    },
    {
      title: 'Learn React',
    },
    {
      title: 'Learn Redux',
    }
  ],
  doneTodos: []
};

function reducer(state, action) {
  switch (action.type) {
    case 'ADD_TODO':
      const newState = Object.assign({}, state, {pendingTodos: [...state.pendingTodos, action.newToDo]});
      return newState;
    case 'COMPLETE_TODO': {
      const index = state.pendingTodos.indexOf(action.todo);
      const pendingTodos = [...state.pendingTodos.slice(0, index), ...state.pendingTodos.slice(index + 1)];
      const doneTodos = [...state.doneTodos, action.todo];
      return Object.assign({}, state, {pendingTodos, doneTodos});
    }
    default:
      return state;
  }
}

export function createTodoStore(devTools: DevToolsExtension) {
  const store = createStore(reducer, defaultState,
    compose(applyMiddleware(createLogger()), devTools.isEnabled() ? devTools.enhancer() : f => f));

  return store;
}
