import { Component, OnInit } from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {ToDo} from '../../model/todo.model';
import {ToDoService} from '../../model/todo.service';
import {Observable} from 'rxjs/Observable';
import {IAppState} from '../../../store/todo.store';

@Component({
  templateUrl: './overview.component.html',
  providers: [ToDoService]
})
export class OverviewComponent {

  @select() pendingTodos$: Observable<Array<ToDo>>;
  @select() doneTodos$: Observable<Array<ToDo>>;

  constructor(private ngRedux: NgRedux<IAppState>) {}

  addToDo(todo: ToDo) {
    this.ngRedux.dispatch({
      type: 'ADD_TODO',
      newToDo: {
        title: todo.title,
      }
    });
  }

  completeToDo(todo: ToDo) {
    // todo.completed = true;
    this.ngRedux.dispatch({
      type: 'COMPLETE_TODO',
      todo: todo
    });
  }

}
