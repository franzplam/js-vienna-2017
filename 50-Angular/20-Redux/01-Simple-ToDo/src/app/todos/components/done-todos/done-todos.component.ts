import { Component, OnInit } from '@angular/core';
import {ToDo} from '../../model/todo.model';
import {ToDoService} from '../../model/todo.service';
import {Observable} from 'rxjs/Observable';
import {NgRedux, select} from '@angular-redux/store';
import {IAppState} from '../../../store/todo.store';

@Component({
  selector: 'td-done-todos',
  templateUrl: './done-todos.component.html',
  providers: [ToDoService]
})
export class DoneTodosComponent {

  doneToDos: ToDo[];
  @select() doneTodos$: Observable<Array<ToDo>>;

  constructor(private ngRedux: NgRedux<IAppState>) {}

  removeToDo(todo: ToDo) {
  }

}
