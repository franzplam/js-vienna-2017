import {ToDo} from '../todos/model/todo.model';

export interface IAppState {
  pendingTodos: ToDo[];
  doneTodos: ToDo[];
}

const initialTodos = [
  new ToDo('Learn JavaScript'),
  new ToDo('Learn React'),
  new ToDo('Learn ngrx')
];

export const initialState: { todos: IAppState } = {
  todos: {
    pendingTodos: initialTodos,
    doneTodos: []
  }
};

export function todoReducer(state, action) {
  switch (action.type) {
    case 'ADD_TODO':
      const newState = Object.assign({}, state, {pendingTodos: [...state.pendingTodos, action.newToDo]});
      return newState;
    case 'COMPLETE_TODO': {
      const index = state.pendingTodos.indexOf(action.todo);
      const pendingTodos = [...state.pendingTodos.slice(0, index), ...state.pendingTodos.slice(index + 1)];
      const doneTodos = [...state.doneTodos, action.todo];
      return Object.assign({}, state, {pendingTodos, doneTodos});
    }
    default:
      return state;
  }
}


