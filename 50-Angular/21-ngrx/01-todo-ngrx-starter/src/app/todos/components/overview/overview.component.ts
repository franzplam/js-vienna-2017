import { Component, OnInit } from '@angular/core';
import {ToDo} from '../../model/todo.model';
import {ToDoService} from '../../model/todo.service';
import {Observable} from 'rxjs/Observable';
import {IAppState} from '../../../store/todo.store';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: './overview.component.html',
  providers: [ToDoService]
})
export class OverviewComponent {

  pendingTodos$: Observable<Array<ToDo>>;
  doneTodos$: Observable<Array<ToDo>>;

  constructor(private store: Store<{todos: IAppState}>) {
    this.pendingTodos$ = this.store.select('todos', 'pendingTodos');
  }

  addToDo(todo: ToDo) {
    this.store.dispatch({
      type: 'ADD_TODO',
      newToDo: {
        title: todo.title,
      }
    });
  }

  completeToDo(todo: ToDo) {
    // todo.completed = true;
    this.store.dispatch({
      type: 'COMPLETE_TODO',
      todo: todo
    });
  }

}
