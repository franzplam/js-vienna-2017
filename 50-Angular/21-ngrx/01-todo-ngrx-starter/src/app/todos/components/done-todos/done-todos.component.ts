import { Component, OnInit } from '@angular/core';
import {ToDo} from '../../model/todo.model';
import {ToDoService} from '../../model/todo.service';
import {Observable} from 'rxjs/Observable';
import {IAppState} from '../../../store/todo.store';
import {Store} from '@ngrx/store';

@Component({
  selector: 'td-done-todos',
  templateUrl: './done-todos.component.html',
  providers: [ToDoService]
})
export class DoneTodosComponent {

  doneTodos$: Observable<Array<ToDo>>;

  constructor(private store: Store<{todos: IAppState}>) {
    this.doneTodos$ = this.store.select('todos', 'doneTodos');
  }

  removeToDo(todo: ToDo) {  }

}
