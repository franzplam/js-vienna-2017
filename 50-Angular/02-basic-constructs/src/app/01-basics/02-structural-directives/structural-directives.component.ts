import { Component } from '@angular/core';

@Component({
    selector: 'aw-structural-directives>',
    templateUrl: './structural-directives.component.html',
})
export class StructuralDirectivesComponent {

    characters: Array<ICharacter> = [
        {
            firstName: 'Katniss',
            lastName: 'Everdeen',
            district: 12
        },
        {
            firstName: 'Peeta',
            lastName: 'Mellark',
            district: 12
        },
        {
            firstName: 'Johanna',
            lastName: 'Mason',
            district: 7
        },
        {
            firstName: 'Finnick',
            lastName: 'Odair',
            district: 4
        }
    ];

    showDistrict = true;
}


export interface ICharacter {
    firstName: string;
    lastName: string;
    district: number;
}
