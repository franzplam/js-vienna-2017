import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'aw-form',
    templateUrl: './reactive-form.component.html'
})
export class ReactiveFormComponent {
    private theForm: FormGroup;

    constructor(formBuilder: FormBuilder) {
        this.theForm = formBuilder.group({
            'name': [''],
            'password': ['']
        });
    }

    onSubmit(value: string): void {
        console.log('submitted: ', value, this.theForm.value);
    }

}
