import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

interface CommitData {
  commit: {author: {name: string, date: string}};
}

@Injectable()
export class FirstService {
    constructor(private _http: HttpClient) { }

    getData = () => this._http.get<CommitData[]>('https://api.github.com/repos/angular/angular/commits')
        .map(data => data.map(c => ({ name: c.commit.author.name, date: new Date(c.commit.author.date) })))
        .catch(this.handleError)

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
