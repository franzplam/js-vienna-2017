import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const backendUrl = 'http://localhost:3456/todos';

interface ToDo {
  title: string;
  completed: boolean;
}

@Component({
  selector: 'aw-backend-crud',
  templateUrl: 'backend-crud.component.html'
})
export class BackendCrudComponent {

  ratings: Observable<any>;
  ratingId: number;
  todoText: string;
  completed = false;

  constructor(private _http: HttpClient) {}

  getTodos() {
    this.ratings = this._http
      .get<ToDo>(backendUrl);
  }

  postTodo() {

    const todo = {title: this.todoText, completed: this.completed};

    this._http
      .post(backendUrl, todo)
      .subscribe();
  }

  putTodo() {

    const rating = {title: this.todoText, completed: this.completed};

    this._http
      .put(`${backendUrl}/${this.ratingId}`, rating)
      .subscribe();
  }

  deleteTodo() {
    this._http
      .delete(`${backendUrl}/${this.ratingId}`)
      .subscribe();
  }

}
