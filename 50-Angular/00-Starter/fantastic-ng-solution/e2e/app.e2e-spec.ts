import { FantasticNgPage } from './app.po';

describe('fantastic-ng App', () => {
  let page: FantasticNgPage;

  beforeEach(() => {
    page = new FantasticNgPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
