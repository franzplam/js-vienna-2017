import {Injectable} from '@angular/core';
import {ToDo} from './todo.model';
import {HttpClient} from '@angular/common/http';

// import 'rxjs/Rx'; // Note: we should only include the operators we need to tune package size
import {Observable} from 'rxjs/Observable';

const backendUrl = 'http://localhost:3456/todos';

@Injectable()
export class ToDoService {

  constructor(private http: HttpClient) {
  }

  getTodos(): Observable<ToDo[]> {
    return this.http.get(backendUrl)
      .map((res: any) => res.data.map((r) => {
        const todo = new ToDo(r.title);
        todo.completed = r.completed;
        todo.id = r.id;
        return todo;
      }))
      .catch(this.handleError);
  }

  saveTodo(rating: ToDo) {
    // TODO: Part of the exercise
  }

  updateTodo(todo: ToDo) {
    // TODO: Part of the exercise
  }

  deleteTodo(rating: ToDo) {
    // TODO: Part of the exercise
  }

  private handleError(error: any) {
    const errMsg = error.message || 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}

