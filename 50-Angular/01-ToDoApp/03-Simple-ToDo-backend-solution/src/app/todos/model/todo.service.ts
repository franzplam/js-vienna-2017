import {Injectable} from '@angular/core';
import {ToDo} from './todo.model';
import {HttpClient} from '@angular/common/http';

// import 'rxjs/Rx'; // Note: we should only include the operators we need to tune package size
import {Observable} from 'rxjs/Observable';

const backendUrl = 'http://localhost:3456/todos';

@Injectable()
export class ToDoService {

  constructor(private http: HttpClient) {
  }

  getTodos(): Observable<ToDo[]> {
    return this.http.get(backendUrl)
      .map((response: any) => {
        const data = response.data;
        return data.map((r) => {
          const todo = new ToDo(r.title);
          todo.completed = r.completed;
          todo.id = r.id;
          return todo;
        });
      })
      .catch(this.handleError);
  }

  saveTodo(todo: ToDo): Observable<ToDo> {

    return this.http.post(backendUrl, todo)
      .map((response: any) => {
        const data = response.data;
        const persistedToDo = new ToDo(data.title);
        persistedToDo.completed = data.completed;
        persistedToDo.id = data.id;
        return persistedToDo;
      })
      .catch(this.handleError);
  }

  updateTodo(todo: ToDo): Observable<ToDo> {
    return this.http.put(`${backendUrl}/${todo.id}`, todo)
      .catch(this.handleError);
  }

  deleteTodo(rating: ToDo): Observable<any> {
    return this.http.delete(`${backendUrl}/${rating.id}`);
  }

  private handleError(error: any) {
    const errMsg = error.message || 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}

