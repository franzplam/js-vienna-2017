import * as $ from 'jquery';
import {ITodoItem} from './model';

'use strict';

export default class TodoView{

    private todoInputElem : JQuery;
    private addBtn : JQuery;

    constructor(){
        this.todoInputElem = $('#input');
        this.addBtn = $('#addBtn');

        var self = this;

        var $todoView = $(this); // use the event infrastructure of jQuery
        this.todoInputElem.on('blur', function(){
            $todoView.trigger('todo-entered', self.todoInputElem.val());
        });

        this.addBtn.on('click', function(){
            $todoView.trigger('todo-added');
            //controller.addItem(); // does not work
            //global.todo.controller.addItem(); // would work ...
        });
    }

    render(todo: ITodoItem){
        this.todoInputElem.val(todo.text);
    }
};
