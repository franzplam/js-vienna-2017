'use strict';

// a class would probably make more sense, 
// but this demonstrates the structural typing
export interface ITodoItem {
    text :string,
    created: Date
}

export class Model {
    public todos : Array<ITodoItem>;
    public newTodo : ITodoItem;
    
    constructor(){
        this.todos = [];
        this.newTodo = {text: '', created: new Date()};
    }
}
