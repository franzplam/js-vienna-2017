'use strict';
import * as $ from 'jquery';
import {Model} from './model';
import TodoView from './todoView';
import TodoListView from './todoListView';

export default class Controller {

    constructor(private model: Model, private todoListView: TodoListView, private todoView: TodoView) {
        this.model = model;
        this.todoView = todoView;
        this.todoListView = todoListView;

        $(this.todoView).on('todo-entered', (event, todoText:string) => this.updateTodo(todoText));
        $(this.todoView).on('todo-added', () => this.addItem());
        $(this.todoListView).on('item-removed', (event, index) => this.removeItemAtIndex(index));

        this.todoView.render(this.model.newTodo);
        this.todoListView.render(this.model.todos);
    }

    updateTodo(todoText: string) {
        this.model.newTodo.text = todoText;
        this.model.newTodo.created = new Date();
    }

    addItem() {
        this.model.todos.push(this.model.newTodo);
        this.model.newTodo = { text: '', created: new Date() };
        this.todoView.render(this.model.newTodo);
        this.todoListView.render(this.model.todos);
    }

    removeItemAtIndex(index: number) {
        this.model.todos.splice(index, 1);
        this.todoListView.render(this.model.todos);
    }

}
