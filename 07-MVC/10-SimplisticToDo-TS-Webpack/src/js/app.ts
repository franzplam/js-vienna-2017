'use strict';
import * as $ from 'jquery';
import {Model} from './model';
import TodoView from './todoView';
import TodoListView from './todoListView';
import Controller from './controller';


let model = new Model();
let todoView = new TodoView();
let todoListView = new TodoListView();
new Controller(model, todoListView, todoView);


// SPA basics: Prevent form submit
$('form').on('submit', function (e) { e.preventDefault() });


