const path = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var config = {
	entry:'./src/js/app.js',
	output: {
		path: path.resolve(__dirname,'dist'),
		filename: 'app/bundle.js'
	},
	plugins: [
        new CopyWebpackPlugin([
        	{ from: 'src/styles', to: 'styles'},
        	{ from: './node_modules/bootstrap/dist/css/bootstrap.css', to: 'styles' },
        	{ from: './node_modules/bootstrap/dist/fonts', to: 'fonts' }
		]),
		new HtmlWebpackPlugin({template: 'src/index.html'})
    ],
	devtool: 'source-map',
	devServer: {
        contentBase: "./dist",
	}

};

module.exports = config;
