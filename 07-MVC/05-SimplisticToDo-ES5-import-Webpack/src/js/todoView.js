import * as $ from 'jquery';

export var todoView = {
    init: function () {
        this.todoInputElem = $('#input');
        this.addBtn = $('#addBtn');

        var self = this;

        var $todoView = $(this); // use the event infrastructure of jQuery
        this.todoInputElem.on('blur', function () {
            $todoView.trigger('todo-entered', self.todoInputElem.val());
        });

        this.addBtn.on('click', function () {
            $todoView.trigger('todo-added');
        });
    },
    render: function (todo) {
        this.todoInputElem.val(todo.text);
    }
};
