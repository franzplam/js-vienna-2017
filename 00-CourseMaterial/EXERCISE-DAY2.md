



## Exercise 1: TypeScript from Scratch

Install the TypeScript compiler, write a TypeScript class and inspect the transpiled result:

- Start a new npm project: `npm init`
- Install TypeScript `npm i -D typescript`
- Create the TypeScript configuration: `node_modules/.bin/tsc --init` (or use WebStorm *Create New -> tsconfic.json* and adjust it.
- Create a npm task that transpiles TypeScript to JavaScript with `tsc` 
- Write a ES2015 class and annotate it with type information
- Inspect the transpiled output



## Exercise 2: TypeScript with 3rd Party Libraries

In the example `10-TypeScript/91-3rdParty`:

- Run the example:

		npm install		
		npm start
	
- A browser should open and you see an error at runtime.
- Rename the file `app.js` to `app.ts` ... do you see any changes?
- Install the type definitions for `lodash`:

		npm install @types/lodash
		
- What did change? You should now get a compiler error ...




## Exercise 3: ToDo App with WebPack and TypeScript

Run the project at `07-MVC/10-SimplisticToDo-TS-Webpack`:

	npm install
	npm run build
	npm run serve

Study the source code and the project configuration. Also have a look at the scripts that are loaded by the browser at runtime.  
Note: At runtime there are only ES5 language features used. Typescript is transpiled and modules are resolved at build time.  

Study the ES2015 / TypeScript constructs that are uses in the implementation. 
Start the development server:

	npm start

Change something in the code and see how the page is reloaded. Try to find the advantages of having types at runtime.



## Exercise 4: AJAX with Callbacks

Inspect the example `08-Async/20-Promises/90-Exercise-jQuery-callbacks`.

Start the Node server on the commandline:

	cd server
	npm install
	npm start

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

	http://localhost:3456/word/0
	
The last parameter can be varied between 0 and 8.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use jQuery with callbacks to implement the logic.



## Exercise 5: AJAX with jQuery & Promises

Inspect the example `08-Async/20-Promises/91-Exercise-promises`.

Start the Node server on the commandline:

	cd server
	npm install
	npm start

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

	http://localhost:3456/word/0
	
The last parameter can be varied between 0 and 8.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use the `axios` library and Promises to implement the logic.




## Exercise 6: Plain MVC

 1. Simple Example:  
Extend the example `07-MVC/01-Simplest-jQuery` so that a URL-link is displayed on the page below the framework logo picture.

 2. ToDo List: 
Change the ToDo List application from example `07-MVC/02-SimplisticToDo-NoMVC` to use the MVC-pattern. The goal is to extract the state of the application from the DOM into a JavaScript object model.




## Exercise 7: Modularisation - Plain JavaScript

Use your solution from exercise 1 or `07-MVC/03-SimplisticToDo-MVC-jQuery-solution`.

Split the implementation `src/main.js` into multiple files. Typically each controller, model and view should be put into its own file. 

Use the namespace pattern and IIFEs for isolation. 



## Exercise 8: ToDo App with WebPack and ES2015 Modules

Run the project at `07-MVC/05-SimplisticToDo-ES5-import-Webpack`:

	npm install
	npm run build
	npm run serve

Study the source code and the project configuration. Also have a look at the scripts that are loaded by the browser at runtime.  
Note: At runtime there are only ES5 language features used. The modules are resolved at build time.  

Inspect how the solution is modularized into different files and how the javascript constructs are imported/exported.  

Start the development server:

	npm start

Change something in the code and see how the page is reloaded.




