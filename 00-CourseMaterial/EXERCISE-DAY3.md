# Exercises Angular

## Exercise 1: Creating an Angular App with Angular CLI

### 1.1 Creating the App

Install Angular CLI as a global npm package:

	npm install -g @angular/cli
	
Create a new Angular application:

	ng new awesome-app
	
Inspect the project that has been created. Try to understand the setup (where is the actual source code, where are the artifacts for deployment ...)

### 1.2 Running the App

Serving the app:

	cd awesome-app
	npm start
	
Navigate a Chrome or Firefox to `http://localhost:4200/`. Inspect the app. Open the browser developer tools and inspect the resources the browser actually loads over the network.


### 1.3 Debugging the App

Debug the app in Chrome:

- Open developer Tools
- Open the Sources Tab
- Open the component sources: Hit Ctrl-P and type 'app.component.ts'
- Set a breakpoint in the constructor

Can you change the title of the component through the debugger?  
Optional: Can you debug in Firefox?


### 1.4 Running the App in Internet Explorer

Navigate a Internet Explorer to `http://localhost:4200/`. Inspect the app.  
What is the Problem? Can you fix it?


### 1.5 Creating a Production Build

Create the production artifacts:

	npm run build --prod --build-optimizer
	
Inspect the contents of `dist`.  
The content of `dist` can be served with any webserver.  
We can use `lite-server` as a simple webserver:

	lite-server --baseDir="dist"
	
Open the browser developer tools and inspect the resources the browser actually loads over the network.  
What are the differences to the development build?


Note you can also serve a production build like this:

	ng serve -prod
	
 


### 1.6 Running the Tests

Execute the tests:

	npm run test
	
Inspect the tests in `src/app/app.component.spec.ts`.  
Modify a test so that it fails.  
Can you debug the tests?  
Can you run a single test?



## Exercise 2: Creating your first Component

In the project directory from exercise 1:

	ng generate component hello
	
- Inspect the generated sources/changes.  
- Modify the component to display "Hello World" in the browser.  
- Write a test that checks for this behaviour.  
- Extend the app so that this new "hello"-component is used.
- Run the tests again ...  
- Optional: Extend the test in `app.component.spec.ts` so that it checks that the new component is rendered as a child component.

- Change the `hello`-Component: It should have a input where you can type your name and below the input a "Greeting" with your name should be printed.
- Extend the test for the `hello`-Component so that this behaviour is checked.
- Optional: Write a end-to-end test that chacks this behaviour. (Hint: `npm run e2e`, `./e2e/app.e2e-spec.ts`


## Exercise 3: Creating an application with routing

Create and run a new project with routing:
	
	ng new fantastic-ng --routing
	cd fantastic-ng
	ng g c first
	ng g c second
	npm start
	
Inspect the generated sources.  
Start the app with `npm start`
Add the routes in `src/app/app-routing.module.ts`:


	const routes: Routes = [
		{ path: 'first', component: FirstComponent  },
  		{ path: 'second', component: SecondComponent  }
  	];
  	
Test the app in the browser.  
Add a default route:

	{ path: '', pathMatch: 'full', redirectTo: 'first'  },
	
Add two links in the `app.component` which enable navigation between `first` and `second` component:  

	<a href="/first">First</a>
	<a href="/second">Second</a>

Test the navigation in the browser. Is it working?  
Inspect the network requests in the browser developer tools ...  
Rewrite to use the `routerLink` directive:

	<a routerLink="/first" routerLinkActive="active">First</a>
	<a routerLink="/second" routerLinkActive="active">Second</a>
	
Inspect the difference ...
Optional: add css to highlight the active state

Create a production build and serve it with a simple webserver:

	npm run build
	http-server dist -p 5679
	
Open `http://localhost:5679/` in the browser.  
Refresh the browser ... what went wrong?

Change to `hash-routing` by adding the following line in the `app.module.ts`:

	providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],


## Exercise 4: Angular ToDo App
Begin with the project `50-Angular/01-ToDoApp/01-SimpleToDo`.  
Start the application:

	npm install
	npm start

The projects gives you a skeleton for a simple ToDo application with a single component. The component contains all the needed functionality in the TypeScript class, but the functionality is not yet connected to the template.

### 4.1 Bind the Template to the Component
It is your task to connect the template to the typescript class so that ToDo items can be created and deleted and the corresponding data is displayed in the UI.

### 4.2 Component Architecture
Split the single `overview` component into several components:

- `new-todo` component for the entry
- `todo-list` for the list
- `todo-item` for the display of a single item

The logic for managing should remain in the `overview` component. The new components should be simple presentation components.

### 4.3 Routing
Introduce a second screen which shows the completed items:

- Create a new component for the second screen.
- Introduce a new url route `./done` which shows the second screen.
- Reuse components: `todo-list` and `todo-item`
- On the new screen completed items can be deleted
- Introduce navigation links to navigate between the screens


## Exercise 5: Forms

In the ToDo Application implement a proper form for the entry of a new todo.

Duplicate the component and implement it once with a template driven form and a second time with a reactive form.  
Add a validation that the text should be at least 3 characters.  
As a reference study the examples in `/50-Angular/02-basic-constructs/src/app/02-forms/`.

Optional: Find out how to implement a custom validation rule: The text should begin with a capital letter.




## Exercise 6: Backend Access

The directory `01-ToDoApp/_server` contains a simple API-Server implementing basic CRUD functionality for our ToDo application.
Start the server with the following commands:

	npm install #just once
	npm start
	
You should now get an array with one rating at the url: `http://localhost:3456/todos`.

Your task is now to access this backend API from the ToDo application:
- When the application is loaded, all the ratings should be loaded from the server
- When a rating is added, it should be saved to the server
- When a rating is completed it should be updated on the server
- When a rating is deleted, it should be deleted from the server.

#### Option 1:
Start with the ToDo appplication from exercise 4 or 5.  
Replace the `todo-service` with a service that interacts with the server via the `http`-service of angular.  

#### Option 2:
Start from `50-Angular/01-ToDoApp/03-Simple-ToDo-backend-skeleton`.
This project already loads the ratings from the server when the application is loaded.


Optional: Try to only use `async` pipes, no `subscribe` ...


The API implemented by the REST-Endpoint is described in the table below:


HTTP-Method   | URL (example) 					      | Request-Body
------------- | ------------- 					      |-------------
GET	    		  | http://localhost:3456/todos   |
GET	    		  | http://localhost:3456/todos/1 |
POST		      | http://localhost:3456/todos		| { "title": "Test", "completed": false}
PUT		 	      | http://localhost:3456/todos/1	| { "title": "Test 2", "completed": true}
DELETE		    | http://localhost:3456/todos/1	| 

Note that all responses are wrapped in a response object with a `data` property.
This is a typical security measure of JSON endpoints. See: http://stackoverflow.com/questions/3503102/what-are-top-level-json-arrays-and-why-are-they-a-security-risk



## Exercise 7: Modularization

Extract the "done"-Screen in the ToDo App in its own module.  

As first reference study the forms examples:

- The module in `50-Angular/02-basic-constructs/src/app/02-forms` defines its own routes.
- You should implement the same "pattern".
- Extract a module i.e : `src/app/todos/components/done-todos/done.module.ts` This module should declare the components for the "done screen"
- Extract its own routes i.e. into: `src/app/todos/components/done-todos/done.routing.ts`
- Import the `done.module`in the `app.module`



Optional: The module should be lazy loaded.

As reference study the backend access example.

- The module in `50-Angular/02-basic-constructs/src/app/03-BackendAccess` defines its own routes. What is the difference to the forms examples above.
- Study how the backen module is referenced in `src/app/app-routing.module.ts`



## Exercise 8: Routing

Create a third screen that shows the details of a single todo. The screen should be bookmarkable i.e. the id of the ToDo should be part of the URL.

- As a reference study the components in `src/app/04-routing`
- create a new component for the `todo-details`
- This component should access the current route via the `ActivatedRoute` service
- retrieve the id of the detail from the URL, then get the todo from the service





