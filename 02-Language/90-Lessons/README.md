Install the dependencies:

    npm install

Run the example:

    npm start

A browser should open at `http://localhost:3000/`
