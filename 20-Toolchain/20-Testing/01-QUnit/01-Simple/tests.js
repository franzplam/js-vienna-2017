var calc;
QUnit.module('Calculator', {

	beforeEach: function() {
        calc = new Calculator();
	},
	afterEach: function() {
	}
});

QUnit.test('add method', function(assert) {
	assert.strictEqual(calc.add(1, 2), 3, "Simplest adding");
	assert.strictEqual(calc.add(2, 2), 4, "More adding");
});


QUnit.module('Async Tests');

QUnit.test( "bla", function( assert ) {
var done = assert.async();
setTimeout(function( newPage ) {
	assert.equal( 1, 1 );
	done();
}, 1000);
});
