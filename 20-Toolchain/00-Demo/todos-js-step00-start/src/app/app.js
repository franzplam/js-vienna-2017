var todoForm = document.getElementById('todo-form');

todoForm.addEventListener('submit', addToDo);


function addToDo(){
    var input = document.getElementById('todo-text');
    var textValue = input.value;

    if (textValue) {
        var listItem = document.createElement("li");
        listItem.innerHTML = textValue;

        var button = document.createElement("button");
        button.innerHTML = "X";
        button.addEventListener('click', removeToDo);
        listItem.appendChild(button);

        document.getElementById("todo-list").appendChild(listItem);
        input.value = "";
    }
}

function removeToDo(){
    var item = this.closest('li');
    item.remove();
}
