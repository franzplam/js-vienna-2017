import $ from 'jquery';
import './base';
import '../css/index.css';

var todoForm = document.getElementById('todo-form');

todoForm.addEventListener('submit', addToDo);

console.log('Starting!!!!');


function addToDo(){
    var input = $('#todo-text');
    var textValue = input.val();

    if (textValue) {
        var listItem = document.createElement('li');
        listItem.innerHTML = textValue;

        var button = document.createElement('button');
        button.innerHTML = 'X';
        button.addEventListener('click', removeToDo);
        listItem.appendChild(button);

        document.getElementById('todo-list').appendChild(listItem);
        input.val('');
    }
}

function removeToDo(){
    var item = this.closest('li');
    item.remove();
}
