export class Model {
    ratings = [];
    newRating = this.createNewRating();

    addRating() {
        this.ratings.push(this.newRating);
        this.newRating = this.createNewRating();
        this.notifyChange();
    }

    removeRating(ratingId) {
        const index = this.ratings.findIndex(r => r.id === ratingId);
        if (index >= 0) {
            this.ratings.splice(index, 1);
            this.notifyChange();
        }
    }

    createNewRating() {
        const id = getId();
        return {id, name: '', grade: undefined};
    }

    notifyChange() {
        $(this).trigger('change');
    }
}


function getId() {
    return Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
}
