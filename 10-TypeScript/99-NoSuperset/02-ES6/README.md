TypeScript is not a superset of ES6.  
(see https://github.com/Microsoft/TypeScript/issues/2606)

It is still possible to rename `app.js` to `app.ts` in WebStorm and the site still works. But the TypeScript compiler has errors.