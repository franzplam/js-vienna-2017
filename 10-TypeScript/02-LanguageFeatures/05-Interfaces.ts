{

    interface ITest {
        test: string
    }

    interface IPerson {
        name: string,
        age: number
    }

    class Person implements IPerson {
         name: string = 'Tyler';
        age: number = 42;

        getInfo() {
            console.log(this.name);
            return this.name + this.age;
        }
    }

    const p1: IPerson = new Person();
    const p2: Person = new Person();

    const o = {
        name: 'Tyler',
        age: 42
    };


    // Structural typing
    const p3: IPerson = o;

    function test(pers: IPerson) {

    }

    test(o);

    // TODO:
    // - Optional types
    //

    // Classes can implement interfaces
    class Child implements IPerson, ITest {
        name: string;
        age: number;
        test: string = 'asdf'
    }
    const p4 = new Person();


    // Interfaces are types
    const greeter = (p: IPerson): void => console.log(p.name);
    const producer = (): IPerson => (new Child());

    greeter(p4);
    const p5 = producer();


    // an interface can describe a function
    interface Creator {
        (name: string): IPerson
    }

    const factory: Creator = function (name) {
        return {
            name: name,
            age: 43
        }
    };

    const p6: IPerson = factory('Durden');
}