import {model} from "./Model";
import {StatisticsView} from "./StatisticsView";

export var statisticsController = {
    init : function() {

        var statisticsView = new StatisticsView();

        $(model).on('change', renderViews);

        renderViews(); // initial rendering


        function renderViews() {
            statisticsView.render(model.ratings);
        }
    }
};
