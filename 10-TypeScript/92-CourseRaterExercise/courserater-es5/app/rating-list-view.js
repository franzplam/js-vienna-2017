(function(global) {
    'use strict';

    function RatingListView() {

        var self = this;

        var ratingList = $('#ratings');

        ratingList.on('click', '.remove', function(e){
            $(self).trigger('ratingRemoved', $(e.target).parents('li').data('id'))
        });

        self.render = function (ratings) {

            ratingList.html('');
            for(var i = 0, len = ratings.length; i < len  ; i++ ){
                var rating = ratings[i];
                ratingList.append(
                    '<li class="list-group-item" data-id="' + rating.id +'">' +
                    '<div class="rating pull-xs-left">' +
                    rating.grade +
                    '</div>' +
                    '<span>' +
                        rating.name +
                    '</span>' +
                    '<span class="pull-xs-right">' +
                    '<button class="btn btn-xs btn-danger remove fa fa-trash-o"></button>' +
                    '</span>' +
                    '</li>'
                );
            }

        }

    }

    global.courserater = global.courserater || {};
    global.courserater.RatingListView = RatingListView;

})(window);
