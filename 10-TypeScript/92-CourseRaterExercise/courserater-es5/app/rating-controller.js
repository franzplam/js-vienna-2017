(function(global) {
    'use strict';

    var controller = {
        init : function() {

            var model = global.courserater.model;
            var newRatingView = new global.courserater.NewRatingView();
            var ratingListView = new global.courserater.RatingListView();

            $(newRatingView).on('ratingChanged', updateRating);
            $(newRatingView).on('ratingAdded', addRating);
            $(ratingListView).on('ratingRemoved', removeRating);
            $(model).on('change', renderViews);

            renderViews(); // initial rendering


            function renderViews() {
                newRatingView.render(model.newRating);
                ratingListView.render(model.ratings);
            }

            function updateRating(event) {
                model.newRating.name = event.rating.name;
                model.newRating.grade = event.rating.grade;
            }

            function addRating() {
                model.addRating();
            }

            function removeRating(ratingId) {
                model.removeRating(ratingId);
            }

        }
    };

    global.courserater = global.courserater || {};
    global.courserater.ratingController = controller;

})(window);
