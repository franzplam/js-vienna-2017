enableTab(location.hash);

function enableTab(hash) {
    if (hash === '') hash = '#main';
    $('.container section').hide();
    $(hash).show();

    $('nav [href]').parent().removeClass('active');
    $('[href="' + location.hash + '"]').parent().addClass('active')
}

window.addEventListener('hashchange', function() {
    enableTab(location.hash);

});

courserater.ratingController.init();
courserater.statisticsController.init();
