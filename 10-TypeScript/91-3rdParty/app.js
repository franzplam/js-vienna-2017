const champions = [
    {name: 'Katniss'},
    {name: 'Peeta'},
    {name: 'Johanna'},
    {name: 'Haymitch'},
    {name: 'Konrad'}
];

const characters  = _.filter(champions, c => c.startsWith('K'));
const out = characters.join(' ');

document.write(out);
