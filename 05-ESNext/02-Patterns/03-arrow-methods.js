// NOTE: Class fields are currently a stage-2 proposal

class Person {
    name = 'Anonymous';
    age = 42;

    printSummary = () => {
        console.log(`Name: ${this.name}, Age: ${this.age}`);
    }
}

const pers = new Person();
pers.printSummary();

setTimeout(pers.printSummary);