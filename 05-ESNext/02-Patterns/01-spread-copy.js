// NOTE: Object rest/spread is currently a stage-3 proposal

// shallow copy of objects 
const sourceObject = {a: 1, b:2};
const copiedObject = {...sourceObject};
console.log(copiedObject);
console.log('Same?', sourceObject === copiedObject);


// merge objects into a new object
const sourceObject1 = {a: 1, b:2, c:3};
const sourceObject2 = {c: 5, d:6, e:7};
const mergedObject = {...sourceObject1, ...sourceObject2, f:9};
console.log(mergedObject);

// disassemble an object to discrete values
const sourceObject3 = {a: 1, b:2, not_intersting:3};
const {a, b} = sourceObject3;
console.log(a);
console.log(b);

// copy an object
const sourceObject4 = {a: 1, b:2, c:3};
const {...copy} = sourceObject4;
console.log(copy);
console.log('Same?', sourceObject4 === copy);

// use a shallow object copy in a function
const sourceObject5 = {a: 1, b:2, c:3};
function expectObject({...paramCopy}){
    console.log(paramCopy);
    console.log('Same?', sourceObject5 === paramCopy);
}
expectObject(sourceObject5);