const array = [1,2];
const [one, two] = array; // array destructuring

const obj = {three: 3, four: 4};
const {three, four, five = 0} = obj; // object destructuring
// const {three:five, four:six} = obj; // renaming


console.log(one);
console.log(two);
console.log(three);
console.log(four);
console.log(five);
// console.log(six);
//
//
// var [first,,,,fifth] = ['one', 'two', 'three', 'four', 'five'];
// console.log(first);
// console.log(fifth);
//


/* Project objects on dicrete values */ 

// var persons = [
//     {
//         firstName: "Katniss",
//         lastName: "Everdeen",
//         district: 12
//     },
//     {
//         firstName: "Peeta",
//         lastName: "Mellark",
//         district: 12
//     },
//     {
//         firstName: "Johanna",
//         lastName: "Mason",
//         district: 7
//     },
//     {
//         firstName: "Finnick",
//         lastName: "Odair",
//         district: 4
//     }
// ];

// persons.forEach(({district}) => console.log(district));
// const districts = persons
//                         .map(({district}) => district)
//                         .filter((elem, index, self) => index == self.indexOf(elem)); // get unique values
// console.log('districts:', districts);



/* Pick parameters from param object */

// function callback1({param1, param2}){
//     console.log('params', param1, param2);
// }
// function callback2({param1, param3}){
//     console.log('params', param1, param3);
// }

// function triggerCallbacks(callback1, callback2){
//     const params = {param1: 1, param2: 2, param3: 3};
//     callback1(params);
//     callback2(params);
// }
// triggerCallbacks(callback1, callback2);