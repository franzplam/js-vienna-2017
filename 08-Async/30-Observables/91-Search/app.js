const input = document.getElementById('input');
const searchButton = document.getElementById('search');
const resultList = document.getElementById('result');


var observable = Rx.Observable.fromEvent(input, 'keyup');

observable
    // .do(event => console.log(event.target.value))
    .map(e => e.target.value)
    .debounceTime(300)
    .distinctUntilChanged()
    .flatMap(v => searchWikipedia(v))
    .subscribe(data => displayResults(data[1]));


searchButton.addEventListener('click', () => {
    const term = input.value || 'observable';
   searchWikipedia(term)
       .then(data => displayResults(data[1]));
});


function displayResults(results) {
    resultList.innerHTML = '';
    if (!results) return;
    for (let result of results) {
        const li = document.createElement('li');
        li.innerText = result;
        resultList.appendChild(li);
    }
}

function searchWikipedia(term){
    return new Promise((resolve, reject) => {
        JSONP({
            url: 'https://en.wikipedia.org/w/api.php',
            data: {
                search: term,
                action: 'opensearch',
                format: 'json'
            },
            success: function (data) {

                if (term.length > 4){ // artificially delay response
                    setTimeout(() => resolve(data), 2000)
                }
                else {
                    resolve(data);
                }
            }
        });
    })
}

/*
 Hints:
 - start with `map` to `searchWikipedia`
 - change to `flatMap`
 - `debounceTime`
 - distinctUntilChanged + map event->value
 - -> inconsistencies 3->4->3 letters -> switchMap
 */
