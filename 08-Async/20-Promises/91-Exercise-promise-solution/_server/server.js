var express = require('express');
var morgan = require('morgan');
var _ = require('lodash');


var app = express();

var data = ["The", "quick", "brown", "fox", "jumps", "over", "the", "lazy",  "dog"];

//app.use(express.static(__dirname));
app.listen(3456);
app.use(express.json());
app.use(express.urlencoded());
app.use(morgan('combined')); // configure default log output
console.log('Server running at http://localhost:3456');
console.log('To get the first word, go to http://localhost:3456/word/0');

app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
});

app.get('/word/:index', function (req, res) {

    var index = req.param("index");

    setTimeout(function() {
        res.status(200).send(data[index]);
    }, Math.random() * 3000);
});

app.get('/', function (req, res) {

    var id = req.param("id");
        res.status(200).send('Server running! <br> To get the first word, go to: <a href="http://localhost:3456/word/0">http://localhost:3456/word/0</a>');

});
